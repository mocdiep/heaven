Objective
=========

!["He who knows that enough is enough will always have enough. - Lao Tzu"][enough_quote]

[enough_quote]: img/laotzu2-2x_enough.jpg "https://www.brainyquote.com/quotes/lao_tzu_100247?img=2"

Abundance is the principle of nature. When we understand about that principle,
we can see that we already have more than what we verily need (not what
we want).  Besides that, the more things we give, the more love we receive.
This application is for us to give the things we have and receive love.
This is also for us to receive the things we need and give our love.

HEAVEN is to HavE And VErily Need.

Installation
============

```bash
ENV_FOLDER=env_heaven
python3 -m venv ${ENV_FOLDER} && source ${ENV_FOLDER}/bin/activate && pip install --upgrade pip
git clone git@gitlab.com:mocdiep/heaven.git heaven_root
# or git clone https://gitlab.com/mocdiep/heaven.git heaven_root
cd heaven
ln -s ../${ENV_FOLDER} env
pip install -r requirements.txt

# For development
pip install -r requirements_dev.txt
```

Development Processes
=====================

Concurrently develop requirements and features
----------------------------------------------

* Why: save time, let developers to involve in planning phase
* Steps:
    * Branch `features/reqs` from `develop` for BA to work on feature files
    * Branch `features/<feature_name>` from `develop` for Developer to work on
      steps files
    * Merge `develop` with `features/reqs` for latest requirements
    * Diff `features/<feature_name>` with `develop` then merge for updates
        * Problem when moving files to another folder --> lost text in original
            file
        * Only rename/move files after merging all branchs to `develop`, then
            all branches should start from `develop` again
    * Merge `develop` with `features/<feature_name>` for latest feature

Behavior Driven Development
---------------------------

### Business Analyst

* Edit feature files in features/ folder

### Developer

* Edit step files in features/steps folder then edit source codes, templates
* Keep the steps those are not being work in current sprint out of step files
* With vim
    * Put into ~/.vimrc: `nnoremap <F4> :so .vimrc<cr>`
    * Put `@wip` to Scenario that is being worked, optionally related ones
    * Press `F4` to run test for Scenarios with `@wip`
    * Press `<leader>F4` to run all tests
    * Arrange feature file as left (or above) window, select step that does not
        exist in step file, arrange step file as right (or below) window,
        select end line of a step, then press `<leader>g` (or `<leader>v)` to
        insert the selected step from feature file to step file.

```bash
./djbh.sh
```

* Behave test output from `./djbh.sh` above or `<leader>F4` in vim
```
0 features passed, 1 failed, 0 skipped
12 scenarios passed, 6 failed, 0 skipped
73 steps passed, 0 failed, 7 skipped, 11 undefined
Took 0m0.597s
```

* Output explaination
```
0 features passed, 1 failed, 0 skipped
                   ^---------^--------------- kanban
12 scenarios passed, 6 failed, 0 skipped
                     ^---------^------------- kanban
73 steps passed, 1 failed, 7 skipped, 10 undefined
                 ^         ^----------^------ kanban
                 +--------------------------- wip
Took 0m0.597s
```

* Make sure 100% coverage so that all lines of code can be executed properly
    when needed.

```bash
DJANGO_SETTINGS_MODULE=heaven.dev_settings coverage run; coverage html
browse htmlcov/index.html
```

Rearrange code without impacting works of Developers and BAs
------------------------------------------------------------

* Why: rename/move files may cause losing code when merging feature branches to
    `develop`
* Steps:
    * mv001: Merge `develop` with all feature branches
    * mv002: Perform rename/move files as needed
    * mv003: Start all branches from the `develop`
